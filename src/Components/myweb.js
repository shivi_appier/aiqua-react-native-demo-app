import React, { Component } from 'react';
// import { WebView } from 'react-native-webview';
import { RNAiquaWebView } from 'react-native-aiqua-sdk';

// export default class MyWeb extends Component {

//   _onLoadStart = () => {
//     const aiqJS = `window.aiqMobileSdk={promises:{},logEvent:function(e){try{var t={data:JSON.parse(e),type:"event"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},setCustomKey:function(e){try{var t={data:JSON.parse(e),type:"profile"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},getVersion:function(){var e=this;return new Promise(function(t,r){var i=e.generateUUID();e.promises[i]={resolve:t,reject:r};t("4.2.0")})},generateUUID:function(){var e=(new Date).getTime();return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var r=(e+16*Math.random())%16|0;return e=Math.floor(e/16),("x"==t?r:3&r|8).toString(16)})},resolvePromise:function(e,t,r){r?this.promises[e].reject(t):this.promises[e].resolve(t),delete this.promises[e]}}; true;`;
//     this.webref.injectJavaScript(aiqJS);
//   };

//   _onLoadEnd = () => {
//     const aiqJS = `window.aiqMobileSdk={promises:{},logEvent:function(e){try{var t={data:JSON.parse(e),type:"event"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},setCustomKey:function(e){try{var t={data:JSON.parse(e),type:"profile"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},getVersion:function(){var e=this;return new Promise(function(t,r){var i=e.generateUUID();e.promises[i]={resolve:t,reject:r};t("4.2.0")})},generateUUID:function(){var e=(new Date).getTime();return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var r=(e+16*Math.random())%16|0;return e=Math.floor(e/16),("x"==t?r:3&r|8).toString(16)})},resolvePromise:function(e,t,r){r?this.promises[e].reject(t):this.promises[e].resolve(t),delete this.promises[e]}}; true;`;
//     this.webref.injectJavaScript(aiqJS);
//   };

//   render() {
  	
//     const aiqJS = `window.aiqMobileSdk={promises:{},logEvent:function(e){try{var t={data:JSON.parse(e),type:"event"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},setCustomKey:function(e){try{var t={data:JSON.parse(e),type:"profile"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},getVersion:function(){var e=this;return new Promise(function(t,r){var i=e.generateUUID();e.promises[i]={resolve:t,reject:r};t("4.2.0")})},generateUUID:function(){var e=(new Date).getTime();return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var r=(e+16*Math.random())%16|0;return e=Math.floor(e/16),("x"==t?r:3&r|8).toString(16)})},resolvePromise:function(e,t,r){r?this.promises[e].reject(t):this.promises[e].resolve(t),delete this.promises[e]}};
//                   setTimeout(function() { if(window.aiqMobileSdk){window.alert('hi')} }, 3000); true;`;

//     const jsAlert = `setTimeout(function() { window.alert('hi') }, 2000);`
//     const nativeAlertJS = `setTimeout(function() { window.postMessage('hello') }, 4000);`;

//     // setTimeout(() => {
//     //   this.webref.injectJavaScript(aiqJS);
//     // }, 2000);

//     return (
//       <WebView
//         ref={r => (this.webref = r)}
//         injectedJavaScript={aiqJS}
//         onMessage={event => {
//             alert(event.nativeEvent.data);
//           }}
//         source={{uri: 'https://aiqua-staging-testweb.herokuapp.com/'}}
//         style={{marginTop: 20}}
//         useWebKit={true}
//         domStorageEnabled={true}
//         javaScriptEnabled={true}
//         onLoadStart={this._onLoadStart}
//       />
//     );
//   }
// }

export default class MyWeb extends Component {

  _qgEventListener = (event) => {
    const dataStr = event.nativeEvent.data;
    const data = JSON.parse(dataStr);
    console.log(data);
    // alert(data);

  }

  render() {
    
    // const aiqJS = `window.aiqMobileSdk={promises:{},logEvent:function(e){try{var t={data:JSON.parse(e),type:"event"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},setCustomKey:function(e){try{var t={data:JSON.parse(e),type:"profile"};window.ReactNativeWebView.postMessage(JSON.stringify(t))}catch(e){}},getVersion:function(){var e=this;return new Promise(function(t,r){var i=e.generateUUID();e.promises[i]={resolve:t,reject:r};t("4.2.0")})},generateUUID:function(){var e=(new Date).getTime();return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var r=(e+16*Math.random())%16|0;return e=Math.floor(e/16),("x"==t?r:3&r|8).toString(16)})},resolvePromise:function(e,t,r){r?this.promises[e].reject(t):this.promises[e].resolve(t),delete this.promises[e]}};true;`;
    // setTimeout(() => {
    //   this.webref.injectJavaScript(aiqJS);
    // }, 2000);
    return (
      <RNAiquaWebView
        // injectedJavaScript={aiqJS}
        ref={r => (this.webref = r)}
        onMessage={this._qgEventListener}
        source={{uri: 'https://shiv-production-test.netlify.com/'}}
        // source={{uri: 'https://aiqua-staging-testweb.herokuapp.com/'}}
        // source={{uri: 'https://lotte.vn'}}
        style={{marginTop: 20}}
        // cacheEnabled={false}
        // onShouldStartLoadWithRequest={request => {
          // Only allow navigating within this website
          // return request.url.startsWith('https://www.citiesocial.com/');
        // }}
        // onShouldStartLoadWithRequest={false}
      />
    );
  }
}