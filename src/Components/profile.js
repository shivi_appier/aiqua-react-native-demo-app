import React from 'react';
import { Alert, StyleSheet, Text, View, TouchableOpacity, WebView } from 'react-native';
import RNAiqua, { AiquaConfigType } from 'react-native-aiqua-sdk';

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btnText: 'def btn',
      btnColor: '#ABCDEF'
    }
  }

  componentDidMount() {
    RNAiqua.getAllPersonalizedConfigForComponent({
      component: 'ProfileScreen',
      configKeys: [
        {
          key: 'btnText',
          type: AiquaConfigType.TEXT,
          defaultValue: this.state.btnText
        },
        {
          key: 'btnColor',
          type: AiquaConfigType.COLOR,
          defaultValue: this.state.btnColor
        }
      ]
    }).then(response => {
      console.log(`got all config: ${JSON.stringify(response)}`);
      response.map(item => {
        this.setState({
          [item.key]: item.value
        });
      });
    }).catch(error => {
      console.log(error);
    })
  }

  static navigationOptions = {
    title: 'Profile Page',
  };

  _onPressButton = () => {
    Alert.alert('You are on second screen');
    RNAiqua.logEvent('react-button-tap');

    this.props.navigation.navigate('Web');
  }

  _onPressBack = () => {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{width: 50, height: 50, backgroundColor: this.state.btnColor}} />
        <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
        <TouchableOpacity onPress={this._onPressButton}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>{this.state.btnText}</Text>
          </View>
        </TouchableOpacity>
        <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
        <View>
          <TouchableOpacity onPress={this._onPressBack}>
            <View style={styles.btn}>
              <Text style={styles.btnText}>Go Back!</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  btn: {
    marginBottom: 30,
    width: 200,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2196F3'
  },
  btnText: {
    color: '#fff'
  } 
});
