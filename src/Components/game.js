import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    TouchableOpacity, 
    Alert, 
    Button
} from 'react-native';
//import { MaterialCommunityIcons as Icon} from 'react-native-vector-icons';}

export default class Tictactoe extends React.Component {

    static navigationOptions = {
        title: 'Tictactoe',
    };

    constructor(props) {
        super(props);

        this.state = {
            gameState: [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
            ],
            currentPlayer: 1,
        }
    }

    ComponentDidmount() {
        this.initializeGame();
    }

    onNewGamePress = () => {
        this.initializeGame();
    };

    initializeGame = () => {
        this.setState({
            gameState: [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
            ],
            currentPlayer: 1,
        });
    };

    getWinner = () => {
        const NUM_TILES = 3;
        const PLAYER_1 = 1;
        const PLAYER_2 = 2;
        const DRAW = -1;
        let arr = this.state.gameState;
        let sum = 0;


        // case with each row
        for (let i = 0; i < NUM_TILES; i++) {
            sum = arr[i][0] + arr[i][1] + arr[i][2];
            if (sum == 3) {
                return PLAYER_1; 
            } else if (sum == -3) { 
                return PLAYER_2; 
            }
        }

        // case for each column
        for (let i = 0; i < NUM_TILES; i++) {
            sum = arr[0][i] + arr[1][i] + arr[2][i];
            if (sum == 3) { 
                return PLAYER_1; 
            } else if (sum == -3) { 
                return PLAYER_2;
            }
        }

        // diagonal
        sum = arr[0][0] + arr[1][1] + arr[2][2];
        if (sum == 3) { 
            return PLAYER_1;
        } else if (sum == -3) { 
            return PLAYER_2;
        }

        // diagonal
        sum = arr[2][0] + arr[1][1] + arr[0][2];
        if (sum == 3) { 
            return PLAYER_1;
        } else if (sum == -3) { 
            return PLAYER_2;
        }

        // draw condition check if any of the value is yet to be filled
        const shouldContinue = arr.some(row => row.some(column => column === 0));

        // it's a draw
        if (shouldContinue) {
            return 0;
        }

        return DRAW;
    };

    onTilePress = (row, col) => {
        const value = this.state.gameState[row][col];
        // return if tile already pressed
        if (value !== 0) {
            return
        }

        const currentPlayer = this.state.currentPlayer;
        const nextPlayer = -currentPlayer;
        let arr = this.state.gameState.slice();
        arr[row][col] = currentPlayer;
        
        this.setState({
            gameState: arr,
            currentPlayer: nextPlayer
        });

        const winner = this.getWinner();
        if (winner == 1 || winner == 2) {
            Alert.alert(`Player ${winner} is the winner`);
            this.initializeGame();
        } else if (winner == -1) {
            Alert.alert(`This is a Draw!`);
            this.initializeGame();
        }
    };

    renderText = (row, col) => {
        const value = this.state.gameState[row][col];
        switch(value)
        {
            case 1: 
                return <Text style={styles.tileX}>X</Text>;
            case -1: 
                return <Text style={styles.tile0}>0</Text>;
            default: 
                return null;
        }
    };

    createColumns = (rowIndex, columns) => {
        let columnTiles = [];
        for (let column = 0; column < columns; column++) {
            columnTiles.push(
                <TouchableOpacity
                    key={column}
                    style={styles.tile}
                    onPress={() => this.onTilePress(rowIndex, column)}>
                    {this.renderText(rowIndex, column)}
                </TouchableOpacity>
            );
        }
        return columnTiles;
    };

    createRows = (rows, columns) => {
        let rowTiles = [];
        for (let row = 0; row < rows; row++) {
            rowTiles.push(
                <View
                    key={row} 
                    style={{
                        flexDirection: 'row', 
                        backgroundColor: 'powderblue'}}>
                    {this.createColumns(row, columns)}
                </View>
            );
        }

        return rowTiles;
    };

    render() {
        return (
            <View style={styles.container}>
                {this.createRows(3, 3)}
                <View style={styles.button}>
                    <Button 
                        title="New Game" 
                        onPress={this.onNewGamePress} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'skyblue',
    },
    tile: {
        flex: 1,
        width: 120,
        height: 120,
        borderWidth: 5,
        alignItems: 'center',
    },
    tileX: {
        color: 'red',
        fontSize: 100,
    },
    tile0: {
        color: 'green',
        fontSize: 100,
    },
    button: {
        marginTop: 100
    }
});