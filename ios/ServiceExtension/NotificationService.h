//
//  NotificationService.h
//  ServiceExtension
//
//  Created by Shiv.Raj on 23/1/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
