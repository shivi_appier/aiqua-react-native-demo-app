import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/Components/home';
import ProfileScreen from './src/Components/profile';
import MyWeb from './src/Components/myweb';
import Tictactoe from './src/Components/game';
import RNAiqua from 'react-native-aiqua-sdk';
// import RNInsider from 'react-native-insider';

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Profile: ProfileScreen,
    Web: MyWeb,
    Game: Tictactoe
  },
  {
    initialRouteName: "Home"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.initialiseSDK();
  }

  componentDidMount() {
    // this.initialiseSDK();
    // if (__DEV__) {
    //   console.log("debug mode");
    // }
  }

  initialiseSDK() {
    RNAiqua.configure({
      appId: 'e26e15e9811a398f6b43', //'bda55ee01e48ea9d4a91',
      appGroup: 'group.org.reactjs.native.example.aiquademo-test.notification', 
      isDev: true // ios dev or prod - default `true`
    });
    RNAiqua.setName('Shiv');

    // RNInsider.init('lottevn', '123', false, 90);
  }

  render() {
    return <AppContainer />;
  }
}